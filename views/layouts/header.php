<?php

use yii\helpers\Html;

?>
<header id="header">
    <div id="logo">
        <?= Html::a(Html::img('/img/завантаження.jpeg', ['class' => 'main-logo', 'alt' => 'company name']), '/') ?>
    </div>
    <div id="block-menu">
        <div style="width: 100%;height: 80px">

        <div id="enter">
            <?php if (Yii::$app->user->isGuest): ?>
                <?= Html::a('<i class="fa fa-sign-in pull-right"></i>Вхід', '/site/login', ['class' => 'btn navbar-btn']) ?>
            <?php else: ?>
                <?= Html::a('Особистий кабінет', '/profile', ['class' => 'btn navbar-btn']) ?>
            <?=Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    '<i class="fa fa-sign-out pull-right"></i>Выйти',
                    ['class' => 'btn']
                )
                . Html::endForm()?>
            <?php endif; ?>
        </div>
        <div id="basket">
            <a href="#">
                <img src="/img/inetrnet-store-cart.png" alt="">
            </a>
        </div>
            <div id="company-name">
                A LOT OF SHOES
            </div>
        </div>

        <div id="menu">
            <ul>
                <li class="active"><a href="#">Каталог взуття</a></li>
                <li><?= Html::a('Оплата та доставка', '/page/delivery') ?></li>
                <li><?= Html::a('Контакти', '/page/contacts') ?></li>
                <li><?= Html::a('Розпродаж', '/page/sale') ?></li>
            </ul>
        </div>

    </div>
    <div class="clearfix"></div>

</header>