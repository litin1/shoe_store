<?php

/* @var $this yii\web\View */

use app\models\Page;

/* @var $page Page*/
$this->title = 'About';
$this->params['breadcrumbs'][] = $page->name;
?>
<div class="page-content">
    <?= $page->content ?>
</div>