<?php

use yii\helpers\Html;
use yii\helpers\Url;

$path = 'http://' . $_SERVER['SERVER_NAME'] . '/img/avatar.jpg';


?>

    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar page-sidebar-fixed scroll">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation ">
            <li class="xn-logo">
                <a href="<?= Url::toRoute([Yii::$app->homeUrl]) ?>"><?= Yii::$app->name ?></a>
                <a href="#" class="x-navigation-control"></a>
            </li>
            <li class="xn-profile">
                <a href="#" class="profile-mini">
                    <img src="<?= $path ?>" alt="John Doe"/>
                </a>
                <div class="profile">
                    <div class="profile-image">
                        <img src="<?= $path ?>" alt="John Doe"/>
                    </div>
                    <div class="profile-data">
                        <div class="profile-data-name"><?php if (!Yii::$app->user->isGuest) echo Yii::$app->user->identity->username ?></div>
                    </div>
                    <div class="profile-controls">
                        <?= Html::a('<span class="fa fa-info"></span>', ['/users/profile'], ['title' => 'Профиль', 'class' => 'profile-control-left']); ?>

                        <?= Html::a('<span class="fa fa-envelope"></span>', ['/users/profile'], ['title' => 'Профиль', 'class' => 'profile-control-right']); ?>
                    </div>
                </div>
            </li>

            <li class="xn-title">Menu</li>


            <li <?php if (Yii::$app->controller->id == 'main') {
                echo 'class="active"';
            } ?>>
                <?= Html::a('<span class="glyphicon glyphicon-signal"></span> <span class="xn-text">Сторінки</span>', ['/admin/page'], []); ?>
            </li>
        </ul>
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->

<?php
