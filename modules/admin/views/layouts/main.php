<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\helpers\Html;

AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        .modal-dialog {
            margin: 2px auto;
            z-index: 1100 !important;
        }
    </style>
</head>
<body>

<?php $this->beginBody() ?>
<div class="page-container">

    <?= $this->render('left.php', []) ?>
    <!-- PAGE CONTENT -->
    <div class="page-content">
        <?= $this->render('header.php', []) ?>
        <div class="content-wrapper">
            <section class="content" style="padding: 15px">
                <? Alert::widget() ?>
                <?= $content ?>
            </section>
        </div>

    </div>
    <!-- END PAGE CONTENT -->
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
