<?php

use app\widgets\Alert;
use yii\helpers\Html;

?>


<ul class="x-navigation x-navigation-horizontal x-navigation-panel">

    <li class="xn-icon-button">
        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
    </li>
    <li class="xn-icon-button pull-right">

        <?= Html::a('<span class="fa fa-sign-out"></span>', ['/site/logout'], ['data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',]) ?>
    </li>

</ul>


<section class="breadcrumb push-down-0">
    <?php if (isset($this->blocks['content-header'])) { ?>
        <h1><?php $this->blocks['content-header'] ?></h1>
    <?php } ?>

</section>

<div class="page-content-wrap">
    <br>
</div>

