<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%page}}`.
 */
class m200531_071138_create_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string()->unique(),
            'name'=>$this->string(),
            'content'=>$this->text(),
            'SEO_title'=>$this->string(255),
            'SEO_keywords'=>$this->string(512),
            'SEO_description'=>$this->string(1024),
            'is_deleted' => $this->boolean(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%page}}');
    }
}
