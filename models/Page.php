<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property int $id
 * @property string|null $slug
 * @property string|null $name
 * @property string|null $content
 * @property string|null $SEO_title
 * @property string|null $SEO_keywords
 * @property string|null $SEO_description
 * @property int|null $is_deleted
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['is_deleted'], 'integer'],
            [['slug', 'name', 'SEO_title'], 'string', 'max' => 255],
            [['SEO_keywords'], 'string', 'max' => 512],
            [['SEO_description'], 'string', 'max' => 1024],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'name' => 'Назва',
            'content' => 'Контент',
            'SEO_title' => 'Seo Title',
            'SEO_keywords' => 'Seo Keywords',
            'SEO_description' => 'Seo Description',
            'is_deleted' => 'Is Deleted',
        ];
    }
}
